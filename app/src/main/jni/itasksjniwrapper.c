#include <jni.h>
#include <string.h>
#include "Clean.h"

int StartAndroid(
        CleanString appName,
        CleanString appPath,
        CleanString sdkPath,
        CleanString storeOpts,
        CleanString libPath);

CleanString newCleanString(int iLength)
{
    CleanString pString = (CleanString)malloc(sizeof(int) + iLength);
    if (pString == NULL) {
        return NULL;
    } else {
        CleanStringLength(pString) = iLength;
        return pString;
    }
}

void freeCleanString(CleanString string)
{
    free(string);
}

JNIEXPORT void JNICALL
Java_nl_ru_cs_clean_itasks_itasks_jni_TaskServer_start(JNIEnv *env, jobject instance,
                                                       jstring appName_, jstring appPath_,
                                                       jstring sdkPath_, jstring storeOpt_,
                                                       jstring libPath_) {
    const char *appName = (*env)->GetStringUTFChars(env, appName_, 0);
    const char *appPath = (*env)->GetStringUTFChars(env, appPath_, 0);
    const char *sdkPath = (*env)->GetStringUTFChars(env, sdkPath_, 0);
    const char *storeOpt = (*env)->GetStringUTFChars(env, storeOpt_, 0);
    const char *libPath = (*env)->GetStringUTFChars(env, libPath_, 0);

    CleanString name = newCleanString(strlen(appName));
    CleanString path = newCleanString(strlen(appPath));
    CleanString sdk = newCleanString(strlen(sdkPath));
    CleanString store = newCleanString(strlen(storeOpt));
    CleanString libLocation = newCleanString(strlen(libPath));

    if (name != NULL && path != NULL && sdk != NULL && store != NULL && libLocation != NULL) {
        strcpy(CleanStringCharacters(name), appName);
        strcpy(CleanStringCharacters(path), appPath);
        strcpy(CleanStringCharacters(sdk), sdkPath);
        strcpy(CleanStringCharacters(store), storeOpt);
        strcpy(CleanStringCharacters(libLocation), libPath);

        StartAndroid(name, path, sdk, store, libLocation);

        freeCleanString(name);
        freeCleanString(path);
        freeCleanString(sdk);
        freeCleanString(store);
        freeCleanString(libLocation);
    }

    (*env)->ReleaseStringUTFChars(env, appName_, appName);
    (*env)->ReleaseStringUTFChars(env, appPath_, appPath);
    (*env)->ReleaseStringUTFChars(env, sdkPath_, sdkPath);
    (*env)->ReleaseStringUTFChars(env, storeOpt_, storeOpt);
    (*env)->ReleaseStringUTFChars(env, libPath_, libPath);
}