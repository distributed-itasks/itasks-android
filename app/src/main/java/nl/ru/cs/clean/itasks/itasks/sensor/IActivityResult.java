package nl.ru.cs.clean.itasks.itasks.sensor;

public interface IActivityResult {
    void onActivityResult(int resultCode, String data);
}
