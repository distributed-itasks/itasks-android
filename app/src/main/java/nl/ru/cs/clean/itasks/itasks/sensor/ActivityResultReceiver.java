package nl.ru.cs.clean.itasks.itasks.sensor;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Get result from a Activity
 */
public class ActivityResultReceiver extends ResultReceiver {
    public static final String KEY_RECEIVER = "KEY_RECEIVER";
    public static final String KEY_DATA = "KEY_DATA";

    private IActivityResult resultReceiver;

    public ActivityResultReceiver() {
        super(null);
    }

    public void setResultReceiver(IActivityResult resultReceiver) {
        this.resultReceiver = resultReceiver;
    }

    static Class<ActivityResultReceiver> CREATOR = ActivityResultReceiver.class;

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        String data = null;
        if (resultCode == Activity.RESULT_OK) {
            data = resultData.getString(KEY_DATA);
        }
        if (resultReceiver != null) {
            resultReceiver.onActivityResult(resultCode, data);
        }
    }
}
