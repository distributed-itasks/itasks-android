package nl.ru.cs.clean.itasks.itasks.sensor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class Camera implements IActivityResult {
    private static final String TAG = "Camera";

    public static final String TAKE_PICTURE_INTENT = "REFRESH_DATA_INTENT";

    private Context context;
    private ITCPSensorClient client;

    public Camera(Context context, ITCPSensorClient client) {
        this.context = context;
        this.client = client;
    }

    public void takePicture() {
        Intent intent = new Intent(TAKE_PICTURE_INTENT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityResultReceiver receiver = new ActivityResultReceiver();
        receiver.setResultReceiver(this);
        intent.putExtra(ActivityResultReceiver.KEY_RECEIVER, receiver);

        context.sendBroadcast(intent);
    }

    @Override
    public void onActivityResult(int resultCode, String data) {
        if (resultCode == Activity.RESULT_OK) {
            client.send("OK " + data + "\n");
        } else {
            client.send("CANCELED\n");
        }
    }
}
