package nl.ru.cs.clean.itasks.itasks.sensor;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class Location implements LocationListener {
    private LocationManager locationManager;
    private ITCPSensorClient client;
    private Context context;

    public Location(Context context, ITCPSensorClient client) {
        this.client = client;
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public void start() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        HandlerThread t = new HandlerThread("location handlerthread");
        t.start();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this, t.getLooper());
    }

    public void stop() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        double lon = location.getLongitude();
        double lat = location.getLatitude();
        Log.i("Location", String.format("Lat: %f, long: %f", lat, lon));
        client.send(String.format("OK %f %f\n", lat, lon));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }
}
