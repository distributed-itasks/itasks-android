package nl.ru.cs.clean.itasks.itasks.jni;

public class TaskServer {
    static {
        System.loadLibrary("itasksjniwrapper");
    }

    public native void start(String appName, String appPath, String sdkPath, String storeOpt, String libPath);
}
