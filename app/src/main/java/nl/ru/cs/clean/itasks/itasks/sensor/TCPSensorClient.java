package nl.ru.cs.clean.itasks.itasks.sensor;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TCPSensorClient implements ITCPSensorClient, Runnable {
    private static final String TAG = "SensorClient";

    private Context context;

    private Socket client;
    private BufferedReader read;
    private PrintWriter write;

    private Camera camera;
    private Orientation orientation;
    private Location location;

    public TCPSensorClient(Socket client, Context context) {
        if (client == null) {
            throw new IllegalArgumentException("Client is null.");
        }
        this.client = client;
        this.context = context;
        try {
            read = new BufferedReader(new InputStreamReader(client.getInputStream()));
            write = new PrintWriter(client.getOutputStream(), false);
        } catch (IOException e) {
            Log.e(TAG, "Communication with clian failed.", e);
        }
    }

    @Override
    public void run() {
        while (client.isConnected()) {
            String request = null;
            try {
                request = read.readLine();
            } catch (IOException e) {
                Log.e(TAG, "Read from client error", e);
            }
            if (request != null) {
                handleRequest(request);
            }
        }
        stop();
    }

    @Override
    public void stop() {
        if (orientation != null) {
            orientation.stop();
        }
        if (location != null) {
            location.stop();
        }
        if (client.isConnected()) {
            try {
                client.close();
            } catch (IOException e) {
            }
        }
    }

    private void handleRequest(String request) {
        if (request.equals("takepicture")) {
            takePicture();
        } else if (request.equals("orientation")) {
            orientation();
        } else if (request.equals("location")) {
            location();
        }
    }

    private void takePicture() {
        if (camera == null) {
            camera = new Camera(context, this);
        }
        camera.takePicture();
    }

    private void orientation() {
        if (orientation == null) {
            orientation = new Orientation(context, this);
            orientation.start();
        }
    }

    public void location() {
        if (location == null) {
            location = new Location(context, this);
            location.start();
        }
    }

    public void send(String message) {
        if (client.isConnected()) {
            write.write(message);
            write.flush();
        }
    }
}
