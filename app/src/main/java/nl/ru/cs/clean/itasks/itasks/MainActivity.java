package nl.ru.cs.clean.itasks.itasks;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.content.res.AssetManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import nl.ru.cs.clean.itasks.itasks.jni.TaskServer;
import nl.ru.cs.clean.itasks.itasks.sensor.ActivityResultReceiver;
import nl.ru.cs.clean.itasks.itasks.sensor.Camera;
import nl.ru.cs.clean.itasks.itasks.sensor.SensorService;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private static final int REQUEST_IMAGE_CAPTURE = 1;

    private static boolean ALWAYS_COPY_ASSERTS = true; // Disable for production.
    private static boolean REMOVE_DATA_STARTUP = true; // Remove the data (disable for production).

    private DataUpdateReceiver dataUpdateReceiver;
    private ResultReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            Intent i = new Intent(this, SensorService.class);
            startService(i);

            String loadingHtml = "<p>Copying files to local storage.</p>";

            if (REMOVE_DATA_STARTUP) {
		loadingHtml = "<p>Copying files to local storage and removing prevourse run data (e.g. shares). This is behaviour is enabled in MainActivity.java and ONLY for development. Disable this behaviour for production (or demo) applications.</p>";
            }

            final ClientFragment fragment = (ClientFragment)getFragmentManager()
                    .findFragmentById(R.id.webViewFragment);

            fragment.getWebView().loadData("<html><head><title>Please wait</title></head><body><h1>Please wait</h1>" + loadingHtml + "</body></html>", "text/html", "utf-8");

            final Thread waitThread = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fragment.getWebView().loadUrl("http://127.0.0.1:8080");
                        }
                    });
                }
            };


            final String libPath = getApplicationContext().getApplicationInfo().nativeLibraryDir + "/libitasks.so";
            Thread thread = new Thread() {
                @Override
                public void run() {
                    createLocalStorage();

                    File appMainFolder = new File(getFilesDir().getAbsolutePath() + "/itasks/app/main");
                    File appDataFolder = new File(getFilesDir().getAbsolutePath() + "/itasks/data");

                    if (REMOVE_DATA_STARTUP) {
                        // This is for testing (remove the state).
                        deleState(appDataFolder);
                    }

                    if (!appMainFolder.exists() || ALWAYS_COPY_ASSERTS) {
                        copyWebPublic();
                        copySapl();
                    }

                    waitThread.start();

                    TaskServer server = new TaskServer();
                    server.start("iTasks-Android",
                            appMainFolder.getAbsolutePath(),
                            "", // Empty = look for WebPublic and SAPL in folder of appMain (/itasks/app).
                            appDataFolder.getAbsolutePath(),
                            libPath);
                }
            };
            thread.start();
        }
    }

    void deleState(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);
    }

    void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    @Override
    protected void onResume() {
        if (dataUpdateReceiver == null) dataUpdateReceiver = new DataUpdateReceiver();
        IntentFilter intentFilter = new IntentFilter(Camera.TAKE_PICTURE_INTENT);
        registerReceiver(dataUpdateReceiver, intentFilter);
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (dataUpdateReceiver != null) unregisterReceiver(dataUpdateReceiver);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DataUpdateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Camera.TAKE_PICTURE_INTENT)) {
                receiver = intent.getParcelableExtra(ActivityResultReceiver.KEY_RECEIVER);
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && receiver != null) {
            Bundle resultData = new Bundle();
            if (resultCode == RESULT_OK) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();

                resultData.putString(ActivityResultReceiver.KEY_DATA,
                        Base64.encodeToString(byteArray, Base64.NO_WRAP));
            }
            receiver.send(resultCode, resultData);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void createLocalStorage() {
        String localStorage = getFilesDir().getAbsolutePath();
        File itasksFolder = new File(localStorage + "/itasks");
        File dataFolder = new File(localStorage + "/itasks/data");
        File appFolder = new File(localStorage + "/itasks/app");
        File appMainFolder = new File(localStorage + "/itasks/app/main");

        if (!itasksFolder.exists()) {
            itasksFolder.mkdir();
        }

        if (!dataFolder.exists()) {
            dataFolder.mkdir();
        }

        if (!appFolder.exists()) {
            appFolder.mkdir();
        }

        if (!appMainFolder.exists()) {
            appMainFolder.mkdir();
        }
    }

    public boolean copyWebPublic() {
        AssetManager assetManager = getAssets();

        String localStorage = getFilesDir().getAbsolutePath();
        File webPublicFolder = new File(localStorage + "/itasks/app/WebPublic");
        if (webPublicFolder.exists()) {
            deleteRecursive(webPublicFolder);
        }

        if (webPublicFolder.exists() || webPublicFolder.mkdirs()) {
            copyAssets("WebPublic", webPublicFolder.getAbsolutePath(), assetManager);
            return true;
        }

        return false;
    }

    public boolean copySapl() {
        AssetManager assetManager = getAssets();

        String localStorage = getFilesDir().getAbsolutePath();
        File saplFolder = new File(localStorage + "/itasks/app/sapl");
        if (saplFolder.exists()) {
            deleteRecursive(saplFolder);
        }

        if (saplFolder.exists() || saplFolder.mkdirs()) {
            copyAssets("sapl", saplFolder.getAbsolutePath(), assetManager);
            Log.e("SAPLSAPL", saplFolder.getAbsolutePath());
            return true;
        }

        return false;
    }

    private boolean copyAssets(String path, String target, AssetManager assetManager) {
        String[] files = null;

        try {
            files = assetManager.list(path);
        } catch (IOException e) {
            Log.e(TAG, String.format("Error loading %s folder", path), e);
            return false;
        }

        if (files.length > 0) { // path is a folder.
            File folder = new File(target);
            if (!folder.exists() && !folder.mkdirs()) {
                return false;
            }

            for (String file : files) {
                String targetFile = file.replace("underscore--", "_");
                if (!copyAssets(path + "/" + file, target + "/" + targetFile, assetManager)) {
                    return false;
                }
            }
        } else {
            if (!copyAssert(path, target, assetManager)) {
                return false;
            }
        }
        return true;
    }

    private boolean copyAssert(String path, String target, AssetManager assetManager) {
        InputStream source = null;
        OutputStream destination = null;

        try {
            source = assetManager.open(path);
            destination = new FileOutputStream(target);
            copyFile(source, destination);
            source.close();
            destination.flush();
            destination.close();
        } catch (IOException e) {
            Log.e(TAG, "Error loading assert file", e);
            return false;
        }
        return true;
    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }
}
