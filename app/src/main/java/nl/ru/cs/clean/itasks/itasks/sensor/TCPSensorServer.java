package nl.ru.cs.clean.itasks.itasks.sensor;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class TCPSensorServer implements Runnable {
    private static final String TAG = "SensorTCPServer";

    private Context context;
    private List<ITCPSensorClient> clients;

    private Socket server;

    public TCPSensorServer(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Context is null.");
        }
        this.context = context;
        this.clients = new ArrayList<>();
    }

    @Override
    public void run() {
        ServerSocket server;

        try{
            server = new ServerSocket(20097);
        } catch (IOException e) {
            Log.e(TAG, "Can't open TCP port.", e);
            return;
        }

        Log.i(TAG, "Ready to accept clients");

        while (server != null && !server.isClosed()) {
            Socket client = null;
            try {
                client = server.accept();
            } catch (IOException e) {
                Log.e(TAG, "Can't accept client", e);
            }

            Log.i(TAG, "Client connected.");

            if (client != null) {
                TCPSensorClient sensorClient = new TCPSensorClient(client, context);
                Thread thread = new Thread(sensorClient);
                thread.start();
                clients.add(sensorClient);
            }
        }
    }

    public void stop() {
        for (ITCPSensorClient client : clients) {
            client.stop();
        }
    }
}
