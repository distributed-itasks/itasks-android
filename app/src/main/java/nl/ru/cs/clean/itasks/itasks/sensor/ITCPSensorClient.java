package nl.ru.cs.clean.itasks.itasks.sensor;

/**
 * Sensor TCP Client.
 */
public interface ITCPSensorClient {
    /**
     * Send a message to the client.
     * @param message Message to send.
     */
    void send(String message);

    /**
     * Close the connection with the client.
     */
    void stop();
}
