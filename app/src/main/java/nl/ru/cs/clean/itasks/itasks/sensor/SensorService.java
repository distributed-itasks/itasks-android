package nl.ru.cs.clean.itasks.itasks.sensor;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Allows access to sensors with a TCP interface.
 */
public class SensorService extends Service {
    private static final String TAG = "SensorTCPServer";

    private volatile HandlerThread handlerThread;
    private Handler serviceHandler;

    private TCPSensorServer server;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null; // No support for binding.
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handlerThread = new HandlerThread("SensorService.HandlerThread");
        handlerThread.start();

        serviceHandler = new Handler(handlerThread.getLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        server = new TCPSensorServer(getBaseContext());
        serviceHandler.post(server);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (server != null) {
            server.stop();
        }
        handlerThread.quit();
        super.onDestroy();
    }
}
