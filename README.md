# iTasks for Android

This Android app can run a iTasks application (libitasks.so) and provides a interface for iTasks to Android services. The (HTML5) interface of iTasks is displayed using a WebView component.

## Building

To build this Anroid app `libitasks.so` is needed, `libitasks.so` is just a iTasks application that is compiled for Android. For an example application see: [https://gitlab.science.ru.nl/distributed-itasks/examples](https://gitlab.science.ru.nl/distributed-itasks/examples).

When a iTasks application is build using the [Docker image](https://gitlab.science.ru.nl/distributed-itasks/cross-compiler) the following steps are preformomed automatic, when building manual (using Android Studio) the following steps need to be performed before building the Android app.

1. Copy `libitasks.so` to `app/src/main/jniLibs/armeabi-v7a/`.
2. Copy `sapl` to `app/src/main/asserts/sapl`.
3. Copy `iTasks-SDK/Client` to `app/src/main/asserts/WebPublic`.
4. Copy `iTasks-SDK/Dependencies/clean-sapl/src/clean.f` to `app/src/main/asserts/sapl`.
